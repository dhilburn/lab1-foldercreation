﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;

public class FolderGenerator : MonoBehaviour
{
    [MenuItem("Tool Creation/Create folder")]
    public static void GenerateFolders()
    {
        string folderLocation;

        //AssetDatabase.CreateFolder("Assets", "Materials");
        //folderLocation = Application.dataPath + "/Materials";
        //System.IO.File.WriteAllText(folderLocation + "/folderStructure.txt", "This folder is for storing materials");

        //AssetDatabase.CreateFolder("Assets", "Textures");
        //folderLocation = Application.dataPath + "/Textures";
        //System.IO.File.WriteAllText(folderLocation + "/folderStructure.txt", "This folder is for storing textures");

        //AssetDatabase.CreateFolder("Assets", "Prefabs");
        //folderLocation = Application.dataPath + "/Prefabs";
        //System.IO.File.WriteAllText(folderLocation + "/folderStructure.txt", "This folder is for storing prefabs");

        //AssetDatabase.CreateFolder("Assets", "Scripts");
        //folderLocation = Application.dataPath + "/Scripts";
        //System.IO.File.WriteAllText(folderLocation + "/folderStructure.txt", "This folder is for storing scripts");

        //AssetDatabase.CreateFolder("Assets", "Scenes");
        //folderLocation = Application.dataPath + "/Scenes";
        //System.IO.File.WriteAllText(folderLocation + "/folderStructure.txt", "This folder is for storing scenes");

        //AssetDatabase.CreateFolder("Assets", "Animations");
        //folderLocation = Application.dataPath + "/Animations";
        //System.IO.File.WriteAllText(folderLocation + "/folderStructure.txt", "This folder is for storing raw animations");

        //AssetDatabase.CreateFolder("Assets/Animations", "Animation Controllers");
        //folderLocation = Application.dataPath + "/Animations/Animation Controllers";
        //System.IO.File.WriteAllText(folderLocation + "/folderStructure.txt", "This folder is for storing animation controllers");

        #region Dynamic Folder Generation
        
        AssetDatabase.CreateFolder("Assets", "Dynamic Assets");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets", "Resources");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Animations");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Animations", "Sources");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Animation Controllers");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Effects");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Models");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Models", "Character");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Models", "Environment");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Prefabs");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Prefabs", "Common");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Sounds");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds", "Music");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds", "SFX");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds/Music", "Common");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds/SFX", "Common");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Textures");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Textures", "Common");

        #endregion

        #region Static Folder Generation
        AssetDatabase.CreateFolder("Assets", "Static Assets");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Animations");
        AssetDatabase.CreateFolder("Assets/Static Assets/Animations", "Sources");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Animation Controllers");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Effects");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Models");
        AssetDatabase.CreateFolder("Assets/Static Assets/Models", "Character");
        AssetDatabase.CreateFolder("Assets/Static Assets/Models", "Environment");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Prefabs");
        AssetDatabase.CreateFolder("Assets/Static Assets/Prefabs", "Common");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Scenes");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Sounds");
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds", "Music");
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds", "SFX");
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds/Music", "Common");
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds/SFX", "Common");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Textures");
        AssetDatabase.CreateFolder("Assets/Static Assets/Textures", "Common");
        #endregion

        #region Other Folder Generation
        //AssetDatabase.CreateFolder("Assets", "Editor");
        AssetDatabase.CreateFolder("Assets", "Extensions");
        AssetDatabase.CreateFolder("Assets", "Gizmos");
        AssetDatabase.CreateFolder("Assets", "Plugins");
        AssetDatabase.CreateFolder("Assets", "Scripts");
        AssetDatabase.CreateFolder("Assets/Scripts", "Common");
        AssetDatabase.CreateFolder("Assets", "Shaders");
        AssetDatabase.CreateFolder("Assets", "Testing");
        #endregion

        #region Dynamic File Structure Notes
        System.IO.File.WriteAllText(Application.dataPath + "/Dynamic Assets/Resources/folderStructure.txt", "All dynamic resources go in these folders. Files within the folders explain what goes in them.");
        System.IO.File.WriteAllText(Application.dataPath + "/Dynamic Assets/Resources/Effects/folderStructure.txt", "All dynamic particle effects go here. A new folder must be created for each effect.");
        System.IO.File.WriteAllText(Application.dataPath + "/Dynamic Assets/Resources/Models/folderStructure.txt", "All dynamic raw models go here. Models have a single-looping animation or are non-animated.");
        System.IO.File.WriteAllText(Application.dataPath + "/Dynamic Assets/Resources/Prefabs/folderStructure.txt", "All dynamic prefabs go here. Prefabs used in multiple scenes should go in Common; prefabs used in unique scenes must be put in a new folder for that scene.");
        System.IO.File.WriteAllText(Application.dataPath + "/Dynamic Assets/Resources/Sounds/folderStructure.txt", "All dynamic sounds go here. Music and sound effects go in their respective folders. Sounds used in multiple instances go in common. Sounds used in unique instances must be put in new folders in their respective sound source (new music under music; new effects under SFX).");
        System.IO.File.WriteAllText(Application.dataPath + "/Dynamic Assets/Resources/Textures/folderStructure.txt", "All dynamic textures go here. Textures used in multiple instances go in Common. Textures used in unique instances must be put in a new folder for that instance.");
        #endregion

        #region Static File Structure Notes
        System.IO.File.WriteAllText(Application.dataPath + "/Static Assets/folderStructure.txt", "All static resources go in these folders. Files within the folders explain what goes in them.");
        System.IO.File.WriteAllText(Application.dataPath + "/Static Assets/Effects/folderStructure.txt", "All static particle effects go here. A new folder must be created for each effect.");
        System.IO.File.WriteAllText(Application.dataPath + "/Static Assets/Models/folderStructure.txt", "All static raw models go here. Models have a single-looping animation or are non-animated.");
        System.IO.File.WriteAllText(Application.dataPath + "/Static Assets/Prefabs/folderStructure.txt", "All static prefabs go here. Prefabs used in multiple scenes should go in Common; prefabs used in unique scenes must be put in a new folder for that scene.");
        System.IO.File.WriteAllText(Application.dataPath + "/Static Assets/Scenes/folderStructure.txt", "All scenes go here.");
        System.IO.File.WriteAllText(Application.dataPath + "/Static Assets/Sounds/folderStructure.txt", "All static sounds go here. Music and sound effects go in their respective folders. Sounds used in multiple instances go in common. Sounds used in unique instances must be put in new folders in their respective sound source (new music under music; new effects under SFX).");
        System.IO.File.WriteAllText(Application.dataPath + "/Static Assets/Textures/folderStructure.txt", "All static textures go here. Textures used in multiple instances go in Common. Textures used in unique instances must be put in a new folder for that instance.");
        #endregion

        #region Other Folder File Structure Notes
        System.IO.File.WriteAllText(Application.dataPath + "/Extensions/folderStructure.txt", "All third-party assets/asset packages go here. Standard Assets packages DO NOT go here!");
        System.IO.File.WriteAllText(Application.dataPath + "/Gizmos/folderStructure.txt", "All gizmo scripts go here.");
        System.IO.File.WriteAllText(Application.dataPath + "/Plugins/folderStructure.txt", "All plugin scripts go here.");
        System.IO.File.WriteAllText(Application.dataPath + "/Scripts/folderStructure.txt", "All other scripts go here. Scripts used across multiple scenes/objects should go in common; new folders should be created for scripts unique to certain scenes/types.");
        System.IO.File.WriteAllText(Application.dataPath + "/Shaders/folderStructure.txt", "All shader scripts go here.");
        System.IO.File.WriteAllText(Application.dataPath + "/Testing/folderStructure.txt", "All assets made for testing purposes go here.");
        #endregion

        AssetDatabase.Refresh();
    }
}
